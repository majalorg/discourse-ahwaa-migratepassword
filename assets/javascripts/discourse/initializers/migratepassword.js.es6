import { withPluginApi } from 'discourse/lib/plugin-api';
import { on } from 'ember-addons/ember-computed-decorators';
import showModal from 'discourse/lib/show-modal';

export default {
  name: 'ahwaa-migratepassword-init',
  initialize(container) {
    withPluginApi('0.8.9', api => {
      api.modifyClass('component:site-header', {
        @on('didInsertElement')
        renderMigratepasswordModal() {
          Ember.run.scheduleOnce('afterRender', () => {
            const currentUser = container.lookup('current-user:main');

            if (currentUser && currentUser.migratepassword_policy) {
              showModal('migratepassword');
            }
          });
        },
      });
    });
  },
};
