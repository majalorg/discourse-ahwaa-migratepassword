import ModalFunctionality from 'discourse/mixins/modal-functionality';

export default Ember.Controller.extend(ModalFunctionality, {
  passwordProgress: null,

  actions: {
    changePassword() {
      if (!this.get('passwordProgress')) {
        this.set(
          'passwordProgress',
          I18n.t('user.change_password.in_progress')
        );
        return Discourse.User.current()
          .changePassword()
          .then(() => {
            // password changed
            this.setProperties({
              changePasswordProgress: false,
              passwordProgress: I18n.t('user.change_password.success'),
            });
          })
          .catch(() => {
            // password failed to change
            this.setProperties({
              changePasswordProgress: false,
              passwordProgress: I18n.t('user.change_password.error'),
            });
          });
      }
    },
  },
});
