# name: discourse-ahwaa-migratepassword
# about: enable password from the old platform to work, based on https://github.com/discoursehosting/discourse-migratepassword
# version: 0.1
# authors: Orlando Del Aguila <orlando@hashlabs.com>
# url: https://gitlab.com/majalorg/discourse-ahwaa-migratepassword

enabled_site_setting :ahwaa_migratepassword_enabled

after_initialize do
  module OldPasswordValidator
    def confirm_password?(password)
      return true if super
      return false unless custom_fields.key?('import_pass')

      hashed_password = custom_fields['import_pass']
      hash, salt = hashed_password.split(':', 2)

      return false unless hash && salt

      if OldPasswordValidator.encrypt_token(password, salt) == hash
        self.password = password
        custom_fields.delete('import_pass')

        OldPasswordValidator.handle_insecure_password(self) if password_validator && errors.any?

        save(validate: false)
      else
        false
      end
    end

    def self.encrypt_token(token, salt)
      hash = salt

      10.times do |i|
        hash = Digest::SHA1.hexdigest([token, i, hash, salt].join)
      end

      hash
    end

    def self.handle_insecure_password(user)
      # set field to render modal in front-end
      user.custom_fields['migratepassword_policy'] = true

      # create and send password reset email
      email_token = user.email_tokens.create(email: user.email)
      Jobs.enqueue(:critical_user_email, type: :forgot_password, user_id: user.id, email_token: email_token.token)
    end
  end

  class ::User
    prepend OldPasswordValidator
  end

  module MigratepasswordAfterPasswordReset
    def logon_after_password_reset
      @user.custom_fields.delete('migratepassword_policy')
      @user.save
      super
    end
  end

  ::UsersController.class_eval do
    prepend MigratepasswordAfterPasswordReset
  end

  add_to_serializer(:current_user, :migratepassword_policy) { !!object.custom_fields['migratepassword_policy'] }
end
